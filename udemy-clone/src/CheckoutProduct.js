import React from 'react'
import Star from '@material-ui/icons/Star'
import {useStateValue} from './StateProvider'
import './CheckoutProduct.css'
import ReactPlayer from "react-player"

function CheckoutProduct({ id, title, price, rating, video }) {
    const [{}, dispatch] = useStateValue();

    const RemoveFromCart = () => {
        dispatch({
            type: 'REMOVE_FROM_BASKET',
            id: id,
        })
    }

    return (
        <div className="checkoutProduct">
            <div>
            <ReactPlayer url={video} width="600" height="300" controls="controls" autoplay="true"/>
            </div>
            <div className="checkoutProduct__info">
                <p className="checkoutProduct__title">{title}</p>
                <p className="checkoutProduct__price">
                    <small>$</small>
                    <strong>{price}</strong>
                </p>
                <div className="checkoutProduct__rating">
                    {Array(rating) 
                    .fill()    
                    .map((_) => (
                    <p><Star className="product__star" /></p>
                    ))}
                </div>               
                <button onClick={RemoveFromCart}>Remove from cart</button>
            </div>
        </div>
    )
}

export default CheckoutProduct

import firebase from "firebase"

const firebaseApp = firebase.initializeApp ({
        apiKey: "AIzaSyAbPvUO0FFUi12wVIvq0ClzONNyfXPt_fA",
        authDomain: "udemy-clone-884f8.firebaseapp.com",
        databaseURL: "https://udemy-clone-884f8.firebaseio.com",
        projectId: "udemy-clone-884f8",
        storageBucket: "udemy-clone-884f8.appspot.com",
        messagingSenderId: "525414335222",
        appId: "1:525414335222:web:eb2a91d2a9fb92371da2d7",
        measurementId: "G-BV69DX9YBL"
})

const db = firebase.firestore();
const auth = firebase.auth()

export {db, auth}
import React from 'react'
import { useStateValue } from './StateProvider'
import "./Checkout.css"
import CheckoutProduct from "./CheckoutProduct"
import Subtotal from "./Subtotal"

function Checkout() {
    const [{basket}] = useStateValue()

    return (
        <div className="checkout">
            <div className="checkout__left">
            <img 
            className="checkout__ad" 
            src="https://utepprospector.com/wp-content/uploads/2020/04/default-meta-image-1.png" 
            alt="" 
            />
        {basket?.length === 0 ? (
            <div>
                <h2>Your Shopping Basket is empty</h2>
            </div>
        ) : (
            <div> 
            <h2 className="checkout__title"></h2>
                {basket.map(item => (
                    <CheckoutProduct
                        id={item.id}
                        title={item.title}
                        rating={item.rating}
                        price={item.price}
                        video={item.video}
                    />
                ))}
            </div>
        )}
        </div>
        {basket.length > 0 && (
            <div className="checkout__right">
                <Subtotal />
            </div>
        )}
        </div>
    )
}

export default Checkout

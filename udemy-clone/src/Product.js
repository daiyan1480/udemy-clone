import React from 'react'
import "./Product.css"
import Star from '@material-ui/icons/Star'
import { useStateValue } from './StateProvider'
import ReactPlayer from 'react-player'

function Product({ id, title, video, price, rating }) {
    const [{}, dispatch] = useStateValue()
    
    const addToBasket = () => {
        dispatch({
            type: 'ADD_TO_BASKET',
            item: {
                id,
                title,
                video,
                price,
                rating
            }
        })
    }
    
    return (
        <div className="product">
            <div className="product__info">
                <p>{title}</p>
                <p className="product__price">
                    <small>$</small>
                    <strong>{price}</strong>
                </p>
                <div className="product__rating">
                    {Array(rating) 
                    .fill()    
                    .map((_) => (
                    <p><Star className="product__star" /></p>
                    ))}
                </div>
            </div>
            <div > 
            <ReactPlayer url={video} width="600" height="300" controls="controls" autoplay="true"/>   
            </div>       
            <button onClick={addToBasket}>Add to cart</button>
        </div>
    )
}

export default Product

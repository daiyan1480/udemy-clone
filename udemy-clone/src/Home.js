import React from 'react'
import "./Home.css"
import Product from './Product'
import ReactPlayer from "react-player"


function Home() {
    return (
        <div className="home">
            <img className="home-image"
            src="https://www.mickeyshannon.com/images/landscape-photography.jpg" 
            alt=""
            />
            
        <div className="home__row">
            <Product
            id="1"
            title="Nature"
            price={10}
            rating={3}
            video="https://www.youtube.com/watch?v=sVPYIRF9RCQ"
            />
            <Product
            id="2"
            title="Motivational"
            price={12}
            rating={5}
            video="https://www.youtube.com/watch?v=bKxL536zqro"
            />
        </div>
        <div className="home__row">
            <Product
            id="3"
            title="Ocean"
            price={15}
            rating={4}
            video="https://www.youtube.com/watch?v=EzKImzjwGyM"
            />
            <Product
            id="4"
            title="Anime"
            price={11}
            rating={5}
            video="https://www.youtube.com/watch?v=tCRbVEGHZlQ"
            />
            <Product
            id="5"
            title="Man"
            price={20}
            rating={3}
            video="https://www.youtube.com/watch?v=WfGMYdalClU"
            />
        </div>
        <div className="home__row">
            <Product
            id="6"
            title="Tricks"
            price={10}
            rating={5}
            video="https://www.youtube.com/watch?v=H9154xIoYTA"
            />
        </div>
      
        </div>
    )
}

export default Home
